package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  //
  // to complete with orGates and demux...
  //

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay) { output.setSignal(a1Sig | a2Sig) }
    }
    a1 addAction orAction
    a2 addAction orAction
  }
  
  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val notIn1, notIn2, notOut = new Wire
    inverter(a1, notIn1)
    inverter(a2, notIn2)
    andGate(notIn1, notIn2, notOut)
    inverter(notOut, output)
  }

//  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
//	???
//  }
  
	def demux(in: Wire, c: List[Wire], out: List[Wire]) {
        if (c.isEmpty) {
            in addAction {
                () => out.head.setSignal(in.getSignal)
            }
        }
        else {
            val out0 = new Wire
            val out1 = new Wire
            val not = new Wire
            inverter(c.head, not)
            andGate(c.head, in, out1)
            andGate(not, in, out0)
            val outs = out.splitAt(out.length / 2)
            demux(out0, c.tail, outs._1)
            demux(out1, c.tail, outs._2)
        }
	}
}

object Circuit extends CircuitSimulator {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def andGateExample {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  //
  // to complete with orGateExample and demuxExample...
  //
  
  def orGateExample {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    in1.setSignal(true)
    run
    
    in2.setSignal(true)
    run
    
    in1.setSignal(false)
    run
  }
  
  def demuxExample {
    var input = new Wire
    var controlWires = Nil
    var outputWires = List(new Wire)
    
    demux(input, controlWires, outputWires)
    probe("input", input)
    probe("outputWire(0)", outputWires(0))
    
    input setSignal true
    run
    
    input setSignal false
    run
  }

  def demuxExample1 {
    var input = new Wire
    var controlWires = List(new Wire)
    var outputWires = List(new Wire, new Wire)
    
    demux(input, controlWires, outputWires)
    probe("input", input)
    probe("controlWire(0)", controlWires(0))
    probe("outputWire(0)", outputWires(0))
    probe("outputWire(1)", outputWires(1))
    
    input.setSignal(true)
    controlWires(0).setSignal(true)
    run
    controlWires(0).setSignal(false)
    run
  }
  
  def demuxExample2 {
    var input = new Wire
    var controlWires = List(new Wire, new Wire)
    var outputWires = List(new Wire, new Wire, new Wire, new Wire)
    
    demux(input, controlWires, outputWires)
    probe("input", input)
    probe("controlWire(0)", controlWires(0))
    probe("controlWire(1)", controlWires(1))
    probe("outputWire(0)", outputWires(0))
    probe("outputWire(1)", outputWires(1))
    probe("outputWire(2)", outputWires(2))
    probe("outputWire(3)", outputWires(3))
    
    input.setSignal(true)
    controlWires(1).setSignal(false)
    controlWires(0).setSignal(true)
    run
    
    controlWires(1).setSignal(true)
    controlWires(0).setSignal(false)
    run
  }  
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
//  Circuit.andGateExample
//  Circuit.orGateExample
//  Circuit.demuxExample
//  Circuit.demuxExample1
  Circuit.demuxExample2
}
