package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import Math._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("min2") = forAll { (a: Int, b: Int) =>
    val h = insert(b, insert(a, empty))
    findMin(h) == min(a, b)
  }
  
  property("empty") = forAll { a: Int =>
    isEmpty(deleteMin(insert(a, empty)))
  }
  
  property("minOfTwoHeaps") = forAll { (h1: H, h2: H) =>
    val h1Min = findMin(h1)
    val h2Min = findMin(h2)
    findMin(meld(h1, h2)) == min(h1Min, h2Min)
  }
  
  property("meld") = forAll { (h1: H, h2: H) =>
    def heapEqual(h1: H, h2: H): Boolean =
      if (isEmpty(h1) && isEmpty(h2)) true
      else {
        val m1 = findMin(h1)
        val m2 = findMin(h2)
        m1 == m2 && heapEqual(deleteMin(h1), deleteMin(h2))
      }
    heapEqual(meld(h1, h2),
              meld(deleteMin(h1), insert(findMin(h1), h2)))
  }
  
  
//  def getSorted(h: H): List[A] = {
//    def getSortedInner(hh:H, l: List[A]): List[A] =  {
//      val n:A = findMin(hh)
//      val nn = deleteMin(hh)
//      getSortedInner(nn, n::l)
//    }
//    getSortedInner(h, Nil)
//  }
//  
//  val heapWithInts: Gen[H] = {
//    for {
//      x <- arbitrary[Int]
//      h <- oneOf(empty, heapWithInts)
//    } yield insert(x, h)
//  }
//  
//  property("getSorted") = forAll { h: H =>
//    val s = getSorted(h)
//    s == List(0,1,2,3,4)
//  }
  
  lazy val genHeap: Gen[H] = for {
    x <- arbitrary[A]
    h <- oneOf(value(empty), genHeap)
  } yield insert(x, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
